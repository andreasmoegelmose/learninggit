# Learning Git
*Andreas Møgelmose, anmo@create.aau.dk, 2020*

Welcome! This is the repository for my Git tutorial, which is a set of Jupyter Notebooks for learning and teaching Git. Get started by opening [the first chapter](https://bbcdn.githack.com/andreasmoegelmose/learninggit/raw/v1.06/LearningGit-Chapter1.html).

## Chapters
* [Chapter 1](https://bbcdn.githack.com/andreasmoegelmose/learninggit/raw/v1.06/LearningGit-Chapter1.html)
* [Chapter 2](https://bbcdn.githack.com/andreasmoegelmose/learninggit/raw/v1.06/LearningGit-Chapter2.html)
* [Chapter 3](https://bbcdn.githack.com/andreasmoegelmose/learninggit/raw/v1.06/LearningGit-Chapter3.html)
