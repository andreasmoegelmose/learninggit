import os

def get_next_tag():
    stream = os.popen('git rev-list --tags --max-count=1')
    tag_hash = stream.read()
    stream = os.popen(f'git describe --tags {tag_hash}')
    latest_tag = stream.read().strip()

    tag_parts = latest_tag.split(".")
    return f"{tag_parts[0]}.{int(tag_parts[1]) + 1:02.0f}"

if __name__ == "__main__":
    os.system(f'git tag -m "New version (autopublish)" {get_next_tag()}')