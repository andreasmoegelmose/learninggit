all: Chapter1 Chapter2 Chapter3 zipfile
.PHONY: all

Chapter2:: reset_remote

Chapter1 Chapter2 Chapter3::
	jupyter nbconvert notebooks/LearningGit-$@.ipynb --execute --output-dir='.'
	python convert_links.py LearningGit-$@.html

reset_remote:
	curl -X DELETE --netrc-file C:\Users\admin\_netrc https://api.bitbucket.org/2.0/repositories/andreasmoegelmose/learninggitdemo
	curl -X POST --netrc-file C:\Users\admin\_netrc https://api.bitbucket.org/2.0/repositories/andreasmoegelmose/learninggitdemo

zipfile:
	rm -f notebooks.zip
	powershell Compress-Archive -Path notebooks/resources, notebooks/LearningGit-Chapter1.ipynb, notebooks/LearningGit-Chapter2.ipynb, notebooks/LearningGit-Chapter3.ipynb -DestinationPath notebooks.zip

publish: all
	python create_readme.py
	git add LearningGit-Chapter*.html README.md notebooks.zip
	git commit -m "New version (autopublish)"
	python create_next_tag.py
	git push
	git push origin --tags