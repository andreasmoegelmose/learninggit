"""Convert .ipynb-links to .html-links.

Usage:
  convert_links.py FILENAME

"""

from docopt import docopt
import re
import fileinput

if __name__ == '__main__':
    arguments = docopt(__doc__)
    print(f"Converting .ipynb-links in {arguments['FILENAME']}...")
    with fileinput.FileInput(arguments['FILENAME'], inplace=True) as file:
      for line in file:
        line = re.sub(r'href="([^"]+).ipynb"', r'href="\1.html"', line)
        print(line, end='')