import os
import create_next_tag

next_tag = create_next_tag.get_next_tag()

with open("README.template.md", "r") as file:
    template = file.read()
    with open("README.md", "w") as output_file:
        print(template.replace("[latest_tag]", next_tag), file=output_file)